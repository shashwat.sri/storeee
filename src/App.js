
import './App.css';
import Counter from './features/counter/Counter';

function App() {
  return (
    <div className="App">
      {/* <input type="text" value={""}/> */}
      <Counter/>
    </div>
  );
}

export default App;
